<?php

namespace App\Http\Controllers;

class ContactController extends Controller
{
    /**
     * Show the application  index page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contact');
    }
}
