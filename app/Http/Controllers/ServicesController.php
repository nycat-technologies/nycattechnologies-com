<?php

namespace App\Http\Controllers;

class ServicesController extends Controller
{
    /**
     * Show the application  index page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('services');
    }
}
