\<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        @include('partials.metatags')

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title> Get In Touch | NYCAT Technologies </title>
<!--        <title>@if (trim($__env->yieldContent('template_title')))@yield('template_title') | @endif {{ config('app.name', Lang::get('titles.app')) }}</title>-->

        {{-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --}}
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        {{-- Fonts --}}
        @yield('template_linked_fonts')

        {{-- Styles --}}
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">

        @yield('template_linked_css')

        {{-- Scripts --}}
        <script>
            window.Laravel = {!! json_encode([
                    'csrfToken' => csrf_token(),
            ]) !!}
            ;
        </script>

        @include('assets.css.css')
    </head>
    <body>
        <!--[if lt IE 10]>
                <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- prelaoder -->
        <!-- <div id="preloader">
        <div class="preloader-wrapper">
            <div class="spinner"></div>
        </div>
        <div class="preloader-cancel-btn">
            <a href="#" class="btn btn-secondary prelaoder-btn">Cancel Preloader</a>
        </div>
        </div> -->
        <!-- END prelaoder -->

        @include('partials.header')
        <!-- inner banenr start -->
        <!--breadcumb start here-->
        <section class="inner-banner-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="inner-banner-content">
                            <h1 class="inner-banner-title">Get in Touch</h1>
                            <ul class="breadcumbs list-inline">
                                <li><a href="{{ url('/') }}">Home</a></li>
                                <li>Contact</li>
                            </ul>
                            <span class="border-divider style-white"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-image" style="background-image:url('images/backgrounds/background-1.jpg')"></div>
        </section>
        <!--breadcumb end here--><!-- inner banenr end -->
        <!-- contact info strart -->
        <section class="xs-section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <div class="xs-heading text-center style4">
                            <h2 class="section-title">Get in Touch</h2>
                            <span class="line"></span>
                            <p>We’re a creative agency that keeps concept and strategy in mind. We’re a team of creative individuals work on bringing ideas to life with imaginative illustrations to shake the digital world.</br> Our highly trained professionals provide complete digital marketing solutions to solve complex problems.</p>
                        </div><!-- .xs-heading END -->
                    </div>
                </div><!-- .row END -->
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <div class="contact-info-wraper">
                            <div class="row">
                                <div class="col-lg-4 col-md-6">
                                    <div class="single-contact-info">
                                        <div class="round-icon">
                                            <i class="icon icon-map-marker2"></i>
                                        </div><!-- .round-icon END -->
                                        <a href="https://www.google.com/maps/place/1400+Herrington+Rd,+Lawrenceville,+GA+30044/@33.9583753,-84.0823087,17z/data=!3m1!4b1!4m5!3m4!1s0x88f5bce77fac5a25:0xba3f9ef83699039!8m2!3d33.9583709!4d-84.08012" target="_blank" class="info-content">3405 Sweetwater Road, Lawrenceville, GA</a>
                                    </div><!-- .single-contact-info END -->
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="single-contact-info">
                                        <div class="round-icon">
                                            <i class="icon icon-phone-call3"></i>
                                        </div><!-- .round-icon END -->
                                        <a href="tel:+1%20(860)%814-7758"><i class="info-content"></i>+1 (860) 814-7758</a>
                                       
                                    </div><!-- .single-contact-info END -->
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="single-contact-info">
                                        <div class="round-icon">
                                            <i class="icon icon-envelope4"></i>
                                        </div><!-- .round-icon END -->
                                        <a href="mailto:info@nycattechnologies.com" class="info-content">info@nycattechnologies.com</a>

                                    </div><!-- .single-contact-info END -->
                                </div>
                            </div><!-- .row END -->
                        </div><!-- .contact-info-wraper END -->
                    </div>
                </div><!-- .row END -->
            </div><!-- .container END -->
        </section><!-- end contact info -->

        <!-- map area strart -->
        <div class="map-area">
            <div class="container">
                <div class="xs-map map" id="map-1"></div>
            </div><!-- .container END -->
        </div><!-- end map area -->

        <!-- contact strart -->
        <section class="xs-section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-md-8 mx-auto">
                        <div class="xs-heading text-center style4">
                            <h3 class="section-title">Drop us a Line</h3>
                            <span class="line"></span>
                            <p>Set a meeting with our professionals or call us today to discuss your project with our team of industry-proven professionals</p>
                        </div><!-- .xs-heading END -->
                    </div>
                </div><!-- .row END -->
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <div class="contact-from-wraper wow fadeIn">
                            <form action="#" class="xs-from" method="post" id="xs-contact-form">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input type="text" name="name" id="xs_contact_name" class="form-control" placeholder="Name *">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input type="email" name="email" id="xs_contact_email" class="form-control" placeholder="Mail *">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input type="number" name="phone" id="xs_contact_phone" class="form-control" placeholder="Phone *">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input type="text" name="subject" id="xs_contact_subject" class="form-control" placeholder="Subject">
                                        </div>
                                    </div>
                                </div><!-- .row END -->
                                <div class="form-group">
                                    <textarea name="massage" id="x_contact_massage" class="form-control" placeholder="Your Message *" cols="30" rows="10"></textarea>
                                </div>
                                <div class="text-center">
                                    <input type="submit" name="submit" value="Submit" class="btn btn-primary style2" id="xs_contact_submit">
                                </div>
                            </form><!-- .xs-from 3xs-contact-form END -->
                        </div><!-- .contact-from-wraper END -->
                    </div>
                </div><!-- .row END -->
            </div><!-- .container END -->
        </section><!-- end contact -->
        @include('partials.sidebar')

        @include('partials.footer')
        {{-- Scripts --}}
        <script src="{{ mix('/js/app.js') }}"></script>
        @include('assets.js.js')

    </body>
</html>
