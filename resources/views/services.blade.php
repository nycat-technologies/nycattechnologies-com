<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        @include('partials.metatags')

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title> Our Services | NYCAT Technologies </title>
<!--        <title>@if (trim($__env->yieldContent('template_title')))@yield('template_title') | @endif {{ config('app.name', Lang::get('titles.app')) }}</title>-->

        {{-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --}}
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        {{-- Fonts --}}
        @yield('template_linked_fonts')

        {{-- Styles --}}
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">

        @yield('template_linked_css')

        {{-- Scripts --}}
        <script>
            window.Laravel = {!! json_encode([
                    'csrfToken' => csrf_token(),
            ]) !!}
            ;
        </script>

        @include('assets.css.css')
    </head>
    <body>
        <!--[if lt IE 10]>
                <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- prelaoder -->
        <!-- <div id="preloader">
        <div class="preloader-wrapper">
            <div class="spinner"></div>
        </div>
        <div class="preloader-cancel-btn">
            <a href="#" class="btn btn-secondary prelaoder-btn">Cancel Preloader</a>
        </div>
        </div> -->
        <!-- END prelaoder -->

        @include('partials.header')
        <!-- inner banenr start -->
        <!--breadcumb start here-->
        <section class="inner-banner-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="inner-banner-content">
                            <h1 class="inner-banner-title">Our Services</h1>
                            <ul class="breadcumbs list-inline">
                                <li><a href="{{ url('/') }}">Home</a></li>
                                <li>services</li>
                            </ul>
                            <span class="border-divider style-white"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-image" style="background-image:url('images/backgrounds/background-1.jpg')"></div>
        </section>
        <!--breadcumb end here--><!-- inner banenr end -->

        <!-- service info section start -->
        <section class="xs-section-padding service-info-section" data-scrollax-parent="true">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 mx-auto">
                        <div class="xs-heading text-center">
                            <h2 class="section-title">ADVANCED WEB & MOBILE <span>SOLUTIONS</span>.</br> ENTERPRISE APPS & PORTALS.</h2>
                            <span class="line"></span>
                        </div><!-- .xs-heading END -->
                    </div>
                </div><!-- .row END -->
                <div class="row service-info-wraper">
                    <div class="col-lg-6">
                        <div class="service-info-summary">
                            <h3>Whether you seek rapid development of a single application or long-term maintenance of your entire suite of applications or corporate systems, we will ensure your technology assets drive exceptional business results.</h3>
                             <ul class="xs-list checkbox">
                                <li>CUSTOM APPLICATION
                                    DEVELOPMENT</li>
                                <li>CUSTOMIZATION OF
                                    THIRD-PARTY PRODUCTS</li>
                                <li>LEGACY APP UPGRADE
                                    AND ENHANCEMENT</li>
                            </ul>
                            <a href="#" class="btn btn-primary">Know more</a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="service-info-img">
                            <img src="{{ asset('images/service-img-1.png') }}" data-scrollax="properties: { translateY: '-20%' }" alt="">
                        </div>
                    </div>
                </div><!-- .row END -->
            </div><!-- .container END -->
        </section><!-- end service info section -->
        <!-- our security section start -->
        <section class="xs-section-padding our-security-section waypoint-tigger" data-scrollax-parent="true">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="service-summary-img">
                            <img src="{{ asset('images/service-summary-1.png') }}" data-scrollax="properties: { translateY: '-20%' }" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="service-summary-text">
                            <div class="xs-heading">
                                <h2 class="section-title">WEB <span>EXPERTISE</span> TO RELY ON</h2>
                                <span class="line"></span>
                            </div><!-- .xs-heading END -->        
                            <p>Build for the mobile-centric web or transform your business to fit in with the modern cross-platform reality with <b>NYCAT</b>. Drawing upon extensive UI/UX and front-end expertise.</p>
                            <div class="piechats-wraper clearfix">
                                <div class="single-piechart">
                                    <div class="chart" data-percent="76">
                                        <div class="chart-content"> 
                                            <p>Achieved</p>
                                        </div>
                                    </div>
                                </div><!-- .single-piechart END -->
                                <div class="single-piechart">
                                    <div class="chart" data-percent="93">
                                        <div class="chart-content"> 
                                            <p>Loaded</p>
                                        </div>
                                    </div>
                                </div><!-- .single-piechart END -->
                                <div class="single-piechart">
                                    <div class="chart" data-percent="85">
                                        <div class="chart-content"> 
                                            <p>Done</p>  
                                        </div>
                                    </div>
                                </div><!-- .single-piechart END -->
                            </div>    
                        </div>
                    </div>
                </div><!-- .row END -->
            </div><!-- .container END -->
        </section><!-- end our security section -->

        <!-- boosting section start -->
        <section class="xs-section-padding boosting-section waypoint-tigger" data-scrollax-parent="true">
            <div class="container">
                <div class="row service-info-wraper">
                    <div class="col-lg-6">
                        <div class="service-summary-text service-summary-2">
                            <div class="xs-heading">
                                <h2 class="section-title">WEB AT SCALE: DESIGN, <span>DEVELOPMENT</span>
AND OPTIMIZATION</h2>
                                <span class="line"></span>
                            </div><!-- .xs-heading END -->        
                            <div class="boosting-lists">
                                <div class="boosting-list">
                                    <span class="count-number"></span>
                                    <p>Functional, convenient and visually compelling HTML5/JavaScript applications that work natively great on mobile and tablet devices.</p>
                                </div><!-- .boosting-list END -->
                                <div class="boosting-list">
                                    <span class="count-number"></span>
                                    <p>Modernization and mobilization of existing web interfaces, integration layers for mobilization of legacy web systems.</p>
                                </div><!-- .boosting-list END -->
                            </div><!-- .boosting-lists END -->
                            <div class="btn-wraper">
                                <a href="#" class="btn btn-primary">Learn More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="service-summary-img">
                            <img src="{{ asset('images/service-summary-2.png') }}" data-scrollax="properties: { translateY: '20%' }" alt="">
                        </div>
                    </div>
                </div><!-- .row END -->
            </div><!-- .container END -->
        </section><!-- end boosting section -->

        @include('partials.sidebar')

        @include('partials.footer')
        {{-- Scripts --}}
        <script src="{{ mix('/js/app.js') }}"></script>
        @include('assets.js.js')

    </body>
</html>
