<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        @include('partials.metatags')

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title> About Our Company | NYCAT Technologies </title>
<!--        <title>@if (trim($__env->yieldContent('template_title')))@yield('template_title') | @endif {{ config('app.name', Lang::get('titles.app')) }}</title>-->

        {{-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --}}
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        {{-- Fonts --}}
        @yield('template_linked_fonts')

        {{-- Styles --}}
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">

        @yield('template_linked_css')

        {{-- Scripts --}}
        <script>
            window.Laravel = {!! json_encode([
                    'csrfToken' => csrf_token(),
            ]) !!}
            ;
        </script>

        @include('assets.css.css')
    </head>
    <body>
        <!--[if lt IE 10]>
                <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- prelaoder -->
        <!-- <div id="preloader">
        <div class="preloader-wrapper">
            <div class="spinner"></div>
        </div>
        <div class="preloader-cancel-btn">
            <a href="#" class="btn btn-secondary prelaoder-btn">Cancel Preloader</a>
        </div>
        </div> -->
        <!-- END prelaoder -->

        @include('partials.header')

        <!-- inner banenr start -->
        <!--breadcumb start here-->
        <section class="inner-banner-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="inner-banner-content">
                            <h1 class="inner-banner-title">About Our Company</h1>
                            <ul class="breadcumbs list-inline">
                                <li><a href="index.html">Home</a></li>
                                <li>About</li>
                            </ul>
                            <span class="border-divider style-white"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-image" style="background-image:url('images/backgrounds/background-1.jpg')"></div>
        </section>
        <!--breadcumb end here--><!-- inner banenr end -->

        <!-- seo info section -->
        <section class="xs-section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 align-self-center">
                        <div class="xs-info-wraper style2 wow fadeInUp">
                            <div class="xs-heading">
                                <h2 class="section-title">New York City and ATlanta <br> linked with the Colombian Software Development Passion</h2>
                                <span class="line"></span>
                            </div>
                            <p>NYCAT Technologies is a company born as a initiative between 2 Colombians software developers ; Andres F. Alvarez (Software developer based in New York City)  and Leonardo Betancourt Caicedo (Software developer based in Atlanta, Georgia) . </p>


                        </div><!-- .xs-info-wraper END -->
                    </div>
                    <div class="col-lg-6">
                        <div class="video-popup-wraper">
                            <div class="xs-info-img">
                                <img src="{{ asset('images/info/info-1.png') }}" alt="">
                            </div>
                            <!--                            <div class="video-content text-center">
                                                            <a href="https://www.youtube.com/watch?v=dnGs2eOE6hQ" class="xs-video-popup video-popup-btn pulse-effect">
                                                                <i class="icon icon-play2"></i>
                                                            </a>
                                                        </div>-->
                        </div>
                    </div>
                </div><!-- .row END -->
            </div><!-- .container END -->
        </section><!-- END seo info section -->

        <!-- working process section -->
        <section class="xs-section-padding primary-bg working-process-anim">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 mx-auto">
                        <div class="xs-heading text-center">
                            <h2 class="section-subtitle">Work Flow</h2>
                            <h3 class="section-title ">Our Working Process</h3>
                        </div><!-- .xs-heading END -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <div class="row no-gutters working-process-group">
                            <div class="col-lg-3 col-md-6">
                                <div class="single-work-process">
                                    <div class="work-process-icon">
                                        <img src="{{ asset('images/work-process/1.png') }}" alt="">
                                    </div>
                                    <h4 class="small">Planning</h4>
                                </div><!-- .single-work-process END -->
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="single-work-process">
                                    <div class="work-process-icon">
                                        <img src="{{ asset('images/work-process/2.png') }}" alt="">
                                    </div>
                                    <h4 class="small">Research</h4>
                                </div><!-- .single-work-process END -->
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="single-work-process">
                                    <div class="work-process-icon">
                                        <img src="{{ asset('images/work-process/3.png') }}" alt="">
                                    </div>
                                    <h4 class="small">Optimizing</h4>
                                </div><!-- .single-work-process END -->
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="single-work-process">
                                    <div class="work-process-icon">
                                        <img src="{{ asset('images/work-process/4.png') }}" alt="">
                                    </div>
                                    <h4 class="small">Results</h4>
                                </div><!-- .single-work-process END -->
                            </div>
                        </div><!-- .row end -->
                    </div>
                </div><!-- .row END -->
            </div><!-- .container END -->
        </section><!-- END working process section -->

        <!-- fun fact section -->
        <section class="xs-section-padding waypoint-tigger">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 mx-auto">
                        <div class="xs-heading style3 wow fadeIn text-center">
                            <h3 class="section-title"><span>Google</span> only loves you <br> when everyone else <span>love</span> you first</h3>
                            <span class="line"></span>
                        </div>
                    </div>
                </div><!-- .row END -->
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <div class="row funfact-wraper no-gutters">
                            <div class="col-md-4">
                                <div class="single-funfact wow fadeIn text-center" data-wow-duration="1s">
                                    <span class="number-percentage-count number-percentage" data-value="32548" data-animation-duration="10000">0</span>
                                    <p>Happy Clients</p>
                                </div><!-- .single-funfact END -->
                            </div>
                            <div class="col-md-4">
                                <div class="single-funfact wow fadeIn text-center" data-wow-duration="1.2s">
                                    <span class="number-percentage-count number-percentage" data-value="4295" data-animation-duration="10000">0</span><span>+</span>
                                    <p>Works Done</p>
                                </div><!-- .single-funfact END -->
                            </div>
                            <div class="col-md-4">
                                <div class="single-funfact wow fadeIn text-center" data-wow-duration="1.4s">
                                    <span class="number-percentage-count number-percentage" data-value="25498" data-animation-duration="10000">0</span>
                                    <p>Subscribers</p>
                                </div><!-- .single-funfact END -->
                            </div>
                        </div><!-- .row end -->
                    </div>
                </div><!-- .row end -->
                <div class="btn-wraper text-center">
                    <a href="#" class="btn btn-primary style2 icon-left"><i class="icon icon-envelope4"></i> Click here to get Subscribe</a>
                </div>
            </div><!-- .container END -->
        </section><!-- END fun fact section -->

        <!-- seo info section -->
        <section class="xs-section-padding gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 align-self-center">
                        <div class="xs-info-wraper style2 wow fadeInUp">
                            <div class="xs-heading">
                                <h3 class="section-title">Our Vision</h3>
                                <span class="line"></span>
                            </div>
                            <p>Satisfy  the needs of the Spanish-speakers public and local businesses in the metropolitan area of ​ New york ,Atlanta and rest of United States of America. Our business offers services such as: Web application development, networks, IT Outsourcing, Android & IOS Apps development, Computer and servers maintenance  and systems vulnerabilities consultant (cibersecurity).</p>
                            <ul class="xs-list check">
                                <li>CUSTOM APPLICATION
                                    DEVELOPMENT</li>
                                <li>CUSTOMIZATION OF
                                    THIRD-PARTY PRODUCTS</li>
                                <li>LEGACY APP UPGRADE
                                    AND ENHANCEMENT</li>
                            </ul>
                            <div class="btn-wraper">
                                <a href="#" class="btn btn-primary style2 icon-right"><i class="icon icon-arrow-right"></i>Get Our Service</a>
                            </div>
                        </div><!-- .xs-info-wraper END -->
                    </div>
                    <div class="col-lg-6">
                        <div class="xs-info-img">
                            <img src="{{ asset('images/info/info-2.png') }}" alt="">
                        </div>
                    </div>
                </div><!-- .row END -->
            </div><!-- .container END -->
        </section><!-- END seo info section -->

        <!-- team section -->
        <section class="xs-section-padding">

            <div class="container">


                <div class="row">
                    <div class="col-md-6 mx-auto">
                        <div class="xs-heading text-center">
                            <h2 class="section-subtitle">Interested!</h2>
                            <h3 class="section-title">Meet Our Team</h3>
                            <span class="line"></span>
                        </div>    
                    </div>
                </div><!-- .row END -->

                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="single-box text-center">
                            <div class="image">
                                <img src="{{ asset('images/team/team-9.jpg') }}" alt="">
                                <div class="hover-area">
                                    <h4 class="title"><a href="#">Andres Alvarez</a></h4>
                                    <p class="description">Co-founder</p>
                                    <span class="line"></span>
                                    <ul class="xs-list list-inline">
                                        <!--<li><a href="https://www.facebook.com/"><i class="icon icon-facebook"></i></a></li>-->
                                        <!--<li><a href="https://twitter.com/"><i class="icon icon-twitter"></i></a></li>-->
                                        <!--<li><a href="https://plus.google.com/discover"><i class="icon icon-google-plus"></i></a></li>-->
                                        <li><a href="" target="_blank"><i class="icon icon-linkedin"></i></a></li>
                                    </ul>
                                </div><!-- .hover-area END -->
                            </div><!-- .image END -->
                            <div class="box-footer">
                                <h4 class="title"><a href="#">Andres Alvarez</a></h4>
                            </div><!-- .box-footer END -->
                        </div><!-- .single-box END -->
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="single-box text-center">
                            <div class="image">
                                <img src="{{ asset('images/team/leo.jpg') }}" alt="">
                                <div class="hover-area">
                                    <h4 class="title"><a href="#">Leonardo Betancourt</a></h4>
                                    <p class="description">Co-Founder</p>
                                    <span class="line"></span>
                                    <ul class="xs-list list-inline">
                                        <!--<li><a href="https://www.facebook.com/"><i class="icon icon-facebook"></i></a></li>-->
                                        <!--<li><a href="https://twitter.com/"><i class="icon icon-twitter"></i></a></li>-->
                                        <!--<li><a href="https://plus.google.com/discover"><i class="icon icon-google-plus"></i></a></li>-->
                                        <li><a href="https://www.linkedin.com/in/leonardo-betancourt-caicedo-45012969/" target="_blank"><i class="icon icon-linkedin"></i></a></li>
                                    </ul>
                                </div><!-- .hover-area END -->
                            </div><!-- .image END -->
                            <div class="box-footer">
                                <h4 class="title"><a href="#">Leonardo Betancourt</a></h4>
                            </div><!-- .box-footer END -->
                        </div><!-- .single-box END -->
                    </div>
                </div><!-- .row END -->
            </div><!-- .container END -->
        </section><!-- END team section -->

        @include('partials.sidebar')

        @include('partials.footer')
        {{-- Scripts --}}
        <script src="{{ mix('/js/app.js') }}"></script>
        @include('assets.js.js')

    </body>
</html>
