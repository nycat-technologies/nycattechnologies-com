<div class="header">
    <!-- topBar section -->
    <div class="xs-top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="xs-top-bar-info">
                        <li>
                            <p>
                                <a href="tel:+1%20(860)%814-7758" aria-label="Phone number 1"><i class="icon icon-phone3"></i>+1 (860) 814-7758</a>
                                
                            </p>

                        </li>

                        <li>
                            <a href="mailto:info@nycattechnologies.com" aria-label="contact us"><i class="icon icon-email"></i>info@nycattechnologies.com</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="xs-list list-inline">
                        <li><a href="https://www.facebook.com/nycattechnologies/" aria-label="Follow us in Facebook"><i class="icon icon-facebook"></i></a></li>
                        <!--<li><a href="https://twitter.com/"><i class="icon icon-twitter"></i></a></li>-->
                        <!--<li><a href="https://plus.google.com/discover"><i class="icon icon-google-plus"></i></a></li>-->
                        <!--<li><a href="https://www.youtube.com/"><i class="icon icon-youtube-v"></i></a></li>-->
                        <li><a href="https://www.linkedin.com/company/nycat-technologies/" aria-label="Follow us in LinkedIn"><i class="icon icon-linkedin"></i></a></li>
                    </ul>
                </div>
            </div><!-- .row END -->
        </div><!-- .container END -->
    </div>
    <!-- End topBar section -->
    <!-- header section -->
    <header class="xs-header header-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="xs-logo-wraper">
                        <a href="{{ url('/') }}" class="xs-logo">
                            <img src="{{ asset('images/branding/logo/logo.webp') }}" alt="company_logo" aria-label="NYCAT Technologies LLC ®">
                        </a>
                    </div>
                </div>
                <div class="col-lg-7 col-md-6">
                    <nav class="xs-menus align-to-right">
                        <div class="nav-header">
                            <a class="nav-brand" href="/"></a>
                            <div class="nav-toggle"></div>
                        </div>
                        <div class="nav-menus-wrapper">
                            <ul class="nav-menu align-to-right">
                                <li>
                                    <a href="{{ url('/') }}">HOME</a>
                                </li>
                                <li>
                                    <a href="{{ url('/about') }}">ABOUT</a>
                                </li>
                                <li>
                                    <a href="{{ url('/services') }}">SERVICES</a>
                                </li>
                                <!--                                <li><a href="#">BLOG</a>
                                                                </li>-->
                                <li><a href="{{ url('/contact') }}">CONTACT</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div class="col-lg-2 col-md-6">
                    <ul class="xs-menu-tools">
                        <li>
                            <a href="#modal-popup-1" class="languageSwitcher-button xs-modal-popup">
                                <span class="xs-flag" style="background-image: url(images/flags/006-united-states.svg);"></span>
                                <span class="lang-title">EN</span>
                            </a>
                        </li>

<!--                        <li>
                            <a href="#modal-popup-2" class="navsearch-button xs-modal-popup"><i class="icon icon-search"></i></a>
                        </li>-->
                    </ul>
                </div>
            </div><!-- .row END -->
            <div class="navSidebar-wraper clearfix">
                <div class="single-navicon">
                    <a href="#" class="navSidebar-button"><i class="icon icon-menu-1"></i></a>
                </div>
            </div><!-- .navSidebar-wraper END -->
        </div><!-- .container END -->
    </header>    <!-- End header section -->
</div>

