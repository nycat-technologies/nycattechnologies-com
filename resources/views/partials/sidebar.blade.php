<!-- sidebar cart item -->
<div class="xs-sidebar-group info-group">
    <div class="xs-overlay black-bg"></div>
    <div class="xs-sidebar-widget">
        <div class="sidebar-widget-container">
            <div class="widget-heading">
                <a href="#" class="close-side-widget">
                    <i class="icon icon-cross"></i>
                </a>
            </div>
            <div class="sidebar-textwidget">
                <div class="sidebar-logo-wraper">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('images/branding/logo/logo.png') }}" alt="sidebar logo">
                    </a>
                </div>
                <p>We’re a creative agency that keeps concept and strategy in mind. We’re a team of creative individuals work on bringing ideas to life with imaginative illustrations to shake the digital world.</br> Our highly trained professionals provide complete digital marketing solutions to solve complex problems.</p>
                <ul class="sideabr-list-widget">
                    <li>
                        <div class="media">
                            <div class="d-flex">
                                <img src="{{ asset('images/location.png') }}" alt="">
                            </div>
                            <div class="media-body">
                                <p>3405 Sweetwater Road</p>
                                <span>Lawrenceville, Georgia</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                    <li>
                        <div class="media">
                            <div class="d-flex">
                                <img src="{{ asset('images/mail.png') }}" alt="">
                            </div>
                            <div class="media-body">
                                <a href="mailto:info@nycattechnologies.com">info@nycattechnologies.com</a>
                                <span>Online Support</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                    <li>
                        <div class="media">
                            <div class="d-flex">
                                <img src="{{ asset('images/phone.png') }}" alt="">
                            </div>
                            <div class="media-body">
                                <p>
                                    <a href="tel:+1%20(860)%814-7758"><i class="icon icon-phone3"></i>+1 (860) 814-7758</a>
                                    <a href="tel:+1%20(678)%724-0778"><i class="icon icon-phone3"></i>+1 (678) 724-0778</a>
                                </p>
                                <span>Mon-Fri 8am-8pm</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                </ul><!-- .sideabr-list-widget -->
                <div class="subscribe-from">
                    <p>Get Subscribed!</p>
                    <form action="#" method="POST" class="subscribe-form">
                        <label for="sub-input"></label>
                        <div class="form-group">
                            <input type="email" name="email" id="sub-input" placeholder="Enter your mail here" class="form-control">
                            <button class="sub-btn" type="submit"><i class="icon icon-arrow-right"></i></button>
                        </div>
                    </form>
                </div>
                <ul class="social-list version-2">
                    <li><a href="https://www.facebook.com/nycattechnologies/" class="facebook"><i class="fa fa-facebook"></i></a></li> 
                    <li><a href="https://www.linkedin.com/company/nycat-technologies/" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                   

                </ul><!-- .social-list -->
                
            </div>
        </div>
    </div>
</div>    <!-- END sidebar widget item --> 