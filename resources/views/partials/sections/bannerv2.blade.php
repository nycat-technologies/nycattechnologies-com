<!-- agency banner section -->
<section class="xs-banner agency-banner4" style="background-image: url(images/welcome/welcome-version-4.webp);">
    <div class="agency-banner-content-group">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mx-auto">
                    <div class="agency-banner-content banner-style4 text-center">
                        <h1 class="banner-sub-title">Top Notch Service</h1>
                        <h2 class="banner-title"><span>Quality</span> is the best Business Plan</h2>
                        <a href="{{ url('/contact') }}" class="btn btn-primary style3">GET STARTED</a>
                    </div><!-- .agency-banner-content END -->
                </div>
            </div><!-- .row END -->  
        </div><!-- .container END -->
    </div><!-- .agency-banner-content-group END -->
</section>
<!-- end agency banner section -->

