<!-- business info section -->
<section class="xs-section-padding business-info-area business-info-area2" data-scrollax-parent="true">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 mx-auto">
                <div class="agency-section-title text-center section-title-style2">                    
                    <h2 class="main-title">Measure of Business</h2>
                    <p style="color:#121c94;">Boost your idea to the next level in the digital era and getting great benefits for your business environment</p>
                </div>
            </div>
        </div><!-- .row end -->
        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="single-info-block text-center">
                    <div class="info-header">
                        <img src="{{ asset('images/info/info-icon4.png') }}" alt="">
                    </div>
                    <h3 class="small">Web Development</h3>
                    <p style="color:#121c94;"> Our team work with the latest web development technologies in the market</p>
                </div><!-- .single-info-block END -->
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="single-info-block text-center active">
                    <div class="info-header">
                        <img src="{{ asset('images/info/info-icon5.png') }}" alt="">
                    </div>
                    <h3 class="small">Creative Design</h3>
                    <p style="color:#121c94;">User experience (UX/UI) is the visual key for get more visits and traffic in your web and mobile app. </p>
                </div><!-- .single-info-block END -->
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="single-info-block text-center">
                    <div class="info-header">
                        <img src="{{ asset('images/info/info-icon6.png') }}" alt="">
                    </div>
                    <h3 class="small">Mobile Application</h3>
                    <p style="color:#121c94;">This resource is very important for deploy your platform through the phones , tablets and mobile devices. </p>
                </div><!-- .single-info-block END -->
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
    <div class="doodle-parallax">
        <img src="{{ asset('images/doodle/parallax-9.png') }}" data-scrollax="properties: { translateY: '-100%' }" class="single-doodle one" alt="">
        <img src="{{ asset('images/doodle/parallax-10.png') }}" data-scrollax="properties: { translateY: '100%' }" class="single-doodle two" alt="">
    </div>
</section><!-- end business info section -->

