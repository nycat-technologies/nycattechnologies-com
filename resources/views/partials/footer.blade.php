<!-- footer section start -->
<footer class="xs-footer-section">
    <div class="container">
        <div class="footer-top-area">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer-logo">
                        <a href="{{ url('/') }}" aria-label="NYCAT Technologies LLC ®">
                            <img src="{{ asset('images/branding/logo/inverter-logo.webp') }}" alt="company_logo_inverter"  height="50" width="200">
                        </a>
                    </div><!-- .footer-logo END -->
                </div>
                <div class="col-md-8">
                    <ul class="address-info-list list-inline">
                        <li>
                            <div class="address-icon">
                                <img src="{{ asset('images/pin.png') }}" alt="">
                            </div>
                            <div class="address-info"><a href="https://www.google.com/maps/place/3405+Sweetwater+Rd,+Lawrenceville,+GA+30044/@33.9472666,-84.1193791,17z/data=!3m1!4b1!4m5!3m4!1s0x88f5a340a740c9a9:0x1b0483f2b625d99f!8m2!3d33.9472622!4d-84.1171904" target="_blank" rel="noopener">3405 Sweetwater Road <br> Lawrenceville , GA </a></div>
                        </li>
                        <li>
                            <div class="address-icon"><img src="{{ asset('images/massage.png') }}" alt=""></div>
                            <div class="address-info"><a href="tel:+1 (860) 814-7758">+1 (860) 814-7758</a> <br> <a href="mailto:info@nycattechnologies.com">info@nycattechnologies.com</a></div>
                        </li>
                    </ul><!-- .address-info-list END -->
                </div>
            </div>
        </div><!-- .footer-top-area END -->
    </div>
    <div class="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="footer-widget">
                        <h4 class="xs-content-title">Company</h4>
                        <ul class="xs-lsit">
                            <li><a href="{{ url('/about') }}">About us</a></li>
                            <li><a href="{{ url('/services') }}">Services</a></li>
                            <li><a href="{{ url('/contact') }}">Contact</a></li>
                        </ul>
                    </div><!-- .footer-widget END -->
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="footer-widget">
                        <h4 class="xs-content-title">Services</h4>
                        <ul class="xs-lsit">
                            <li><a href="#">Customized software development</a></li>
                            <li><a href="#">Consultant services </a></li>
                            <li><a href="#">IT support for business</a></li>
                           <!-- <li><a href="#">Pay Per Click</a></li>
                            <li><a href="#">Email Marketing</a></li>
                            <li><a href="#">Advertisement</a></li> -->
                        </ul>
                    </div><!-- .footer-widget END -->
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="footer-widget">
<!--                        <h4 class="xs-content-title">Articles</h4>-->
                      <!--  <ul class="articles-list">
                            <li>
                                <a class="entry-title" href="blog-single.html">Twice profit than before you ever</a>
                                <span class="entry-meta"><i class="icon icon-clock"></i> January 14, 2018</span>
                            </li>
                            <li>
                                <a class="entry-title" href="blog-single.html">Twice profit than before you ever</a>
                                <span class="entry-meta"><i class="icon icon-clock"></i> January 14, 2018</span>
                            </li>
                            <li>
                                <a class="entry-title" href="blog-single.html">Twice profit than before you ever</a>
                                <span class="entry-meta"><i class="icon icon-clock"></i> January 14, 2018</span>
                            </li>
                        </ul><!-- .articles-list END -->
                    </div><!-- .footer-widget END -->
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="footer-widget">
                        <h4 class="xs-content-title">Request a quote</h4>
                        <form action="#" class="contact-form" method="post">
                            <label for="email">Email</label>
                            <input type="email" name="email" id="email" placeholder="Email" class="form-control">
                            <label for="message">Message</label>
                            <textarea name="message" id="message" class="form-control" placeholder="Message" cols="30" rows="10"></textarea>
                            <input type="submit" value="Submit now" class="submit-btn">
                        </form>
                    </div><!-- .footer-widget END -->
                </div>
            </div>
        </div>
    </div><!-- .footer-main END -->
    <div class="partner-area-wraper" >
        <div class="container">
            <div class="partner-area" style="background-color: #FEFFEA !important;">
                <div class="row">
                    <div class="col-md-8">
                        <ul class="xs-lsit list-inline">
                            <!--<li class="title" color=#fff">Our clients :</li>-->
<!--                            <li><img src="{{ asset('images/partner/dentalnow-logo-en.png')}}"  alt="" height="30" width="86"></li>
                            <li><img src="{{ asset('images/partner/yourcasa-logo-en.png')}}"  alt="" height="20" width="86"></li>-->
                            <!-- <li><img src="{{ asset('images/partner/partner-3.png') }}" alt=""></li> -->
                            <!-- <li><img src="{{ asset('images/partner/partner-4.png') }}" alt=""></li> -->
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="xs-list list-inline text-right">
                            <li><img src="{{ asset('images/card/visa.png') }}" alt=""></li>
                            <li><img src="{{ asset('images/card/master-card.png') }}" alt=""></li>
                            <li><img src="{{ asset('images/card/discover.png') }}" alt=""></li>
                            <li><img src="{{ asset('images/card/american-express.png') }}" alt=""></li>
                        </ul>
                    </div>
                </div>
            </div><!-- .partner-area END -->
        </div>
    </div><!-- partner-area-wraper. END -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="copyright-text">
                        <p>Copyright &copy; <?php echo date('Y'); ?>, All Right Reserved <a href="{{ url('/') }}">NYCAT Technologies LLC</a></p>
                    </div><!-- .copyright-text END -->
                </div>
                <div class="col-md-6">
                    <ul class="social-list">
                        <li><a href="https://www.facebook.com/nycattechnologies/" aria-label="Follow us in Facebook" class="facebook"><i class="fa fa-facebook"></i></a></li>
<!--                        <li><a href="https://twitter.com/" class="twitter"><i class="fa fa-twitter"></i></a></li>-->
                        <li><a href="https://www.linkedin.com/company/nycat-technologies/" aria-label="Follow us in LinkedIn" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
<!--                        <li><a href="https://www.instagram.com/" class="instagram"><i class="fa fa-instagram"></i></a></li>-->
<!--                        <li><a href="https://plus.google.com/discover" class="googlePlus"><i class="fa fa-google-plus"></i></a></li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- .copyright END -->
</footer>
<!-- footer section end -->	
