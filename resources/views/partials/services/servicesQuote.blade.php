<!-- service info block section start -->
<section class="xs-section-padding gray-bg service-info-block-area">
    <div class="container">
        <div class="row">
            <div class="col-md-10 mx-auto">
                <div class="xs-heading text-center">
                    <h2 class="section-title">We can help to build your dreams apps</h2>
                </div><!-- .xs-heading END -->
            </div>
        </div><!-- .row END -->
        <div class="row service-block-group">
            <div class="col-md-6 col-lg-4">
                <div class="service-info-block text-center">
                    <div class="info-block-header">
                        <img src="{{ asset('images/service-info-icon-1.png') }}" alt="">
                    </div>
                    <h3 class="service-info-title">Customized development</h3>
                    <p style="color:#121c94;">Our specialists can you to build application from simple web site to complex application.</p>
                </div><!-- .service-info-block END -->
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="service-info-block text-center">
                    <div class="info-block-header">
                        <img src="{{ asset('images/service-info-icon-2.png') }}" alt="">
                    </div>
                    <h3 class="service-info-title">Organization</h3>
                    <p style="color:#121c94;"> We are qualified  to list the requirements  , deploy and test the product with the customer to watch the the development evolution.  </p>
                </div><!-- .service-info-block END -->
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="service-info-block text-center">
                    <div class="info-block-header">
                        <img src="{{ asset('images/service-info-icon-3.png') }}" alt="">
                    </div>
                    <h3 class="service-info-title">Boost your idea to be success</h3>
                    <p style="color:#121c94;">Your app is  the key to enter to a big digital ecosystems and get the latest benefits from it. </p>
                </div><!-- .service-info-block END -->
            </div>
        </div><!-- .row .service-block-group END -->
        <div class="row">
            <div class="col-md-12">
                <div class="service-img text-center">
                    <img src="{{ asset('images/service-img-2.webp') }}" alt="">
                </div>
            </div>
        </div> 
    </div><!-- .container END -->
</section><!-- end service info block section -->

