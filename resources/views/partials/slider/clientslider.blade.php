<!-- agency client slider area section -->
<div class="agency_client_slider gray-bg">
    <div class="container">
        <div class="client-slider owl-carousel">
            <div class="item">
                <img src="{{ asset('images/client/client-preciousmetalsinternational.svg') }}" alt="">
            </div>
            <div class="item">
                <img src="{{ asset('images/client/client-bohemiarealtygroup.svg') }}" alt="">
            </div>
             <div class="item">
                <img src="{{ asset('images/client/client-oronightclub.svg') }}" alt="">
            </div>

        </div><!-- .client-slider end -->
    </div><!-- .container END -->
</div>
<!-- agency client slider area section end -->
