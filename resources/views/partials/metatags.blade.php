<meta charset="utf-8">
<meta name="description" content="We’re a creative agency that keeps concept and strategy in mind. We’re a team of creative individuals work on bringing ideas to life with imaginative illustrations to shake the digital world.">
<meta name="author" content="NYCAT TECHNOLOGIES LLC">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="google-site-verification" content="W8LD9pSuyZz9-9kk1VtFznw1tZYbzZJAUOjrxcp4tl4" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/branding/favicon/apple-icon-57x57.png') }}">
<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/branding/favicon/apple-icon-60x60.png') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/branding/favicon/apple-icon-72x72.png') }}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/branding/favicon/apple-icon-76x76.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/branding/favicon/apple-icon-114x114.png') }}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/branding/favicon/apple-icon-120x120.png') }}">
<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/branding/favicon/apple-icon-144x144.png') }}">
<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/branding/favicon/apple-icon-152x152.png') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/branding/favicon/apple-icon-180x180.png') }}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/branding/favicon/android-icon-192x192.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/branding/favicon/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/branding/favicon/favicon-96x96.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/branding/favicon/favicon-16x16.png') }}">
<link rel="manifest" href="{{ asset('images/branding/favicon/manifest.json') }}">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{ asset('images/branding/favicon/ms-icon-144x144.png') }}">
<meta name="theme-color" content="#2567ac">
<meta http-equiv="cache-control" content="max-age=31536000" />