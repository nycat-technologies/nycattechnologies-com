<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900%7COpen+Sans:400,700,700i" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/animate.css') }}">
<link rel="stylesheet" href="{{ asset('css/iconfont.css') }}">
<link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/rev-settings.css') }}">
<!--For Plugins external css-->
<link rel="stylesheet" href="{{ asset('css/plugins.css') }}" />
<!--Theme custom css -->
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<!--Theme Responsive css-->
<link rel="stylesheet" href="{{ asset('css/responsive.css') }}" />

