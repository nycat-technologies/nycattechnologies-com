-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: nycattechnologies
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activations`
--

DROP TABLE IF EXISTS `activations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activations_user_id_index` (`user_id`),
  CONSTRAINT `activations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activations`
--

LOCK TABLES `activations` WRITE;
/*!40000 ALTER TABLE `activations` DISABLE KEYS */;
/*!40000 ALTER TABLE `activations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `laravel2step`
--

DROP TABLE IF EXISTS `laravel2step`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `laravel2step` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `authCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `authCount` int(11) NOT NULL,
  `authStatus` tinyint(1) NOT NULL DEFAULT '0',
  `authDate` datetime DEFAULT NULL,
  `requestDate` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `laravel2step_userid_index` (`userId`),
  CONSTRAINT `laravel2step_userid_foreign` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `laravel2step`
--

LOCK TABLES `laravel2step` WRITE;
/*!40000 ALTER TABLE `laravel2step` DISABLE KEYS */;
/*!40000 ALTER TABLE `laravel2step` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `laravel_logger_activity`
--

DROP TABLE IF EXISTS `laravel_logger_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `laravel_logger_activity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `userType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `route` longtext COLLATE utf8mb4_unicode_ci,
  `ipAddress` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userAgent` text COLLATE utf8mb4_unicode_ci,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` longtext COLLATE utf8mb4_unicode_ci,
  `methodType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `laravel_logger_activity`
--

LOCK TABLES `laravel_logger_activity` WRITE;
/*!40000 ALTER TABLE `laravel_logger_activity` DISABLE KEYS */;
INSERT INTO `laravel_logger_activity` VALUES (1,'Logged In','Registered',1,'http://nycattechnologies.test/login','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/login','POST','2019-01-12 18:26:40','2019-01-12 18:26:40',NULL),(2,'Viewed home','Registered',1,'http://nycattechnologies.test/home','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/login','GET','2019-01-12 18:26:41','2019-01-12 18:26:41',NULL),(3,'Viewed users','Registered',1,'http://nycattechnologies.test/users','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/home','GET','2019-01-12 18:27:26','2019-01-12 18:27:26',NULL),(4,'Viewed users/1','Registered',1,'http://nycattechnologies.test/users/1','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/users','GET','2019-01-12 18:28:38','2019-01-12 18:28:38',NULL),(5,'Viewed users/1/edit','Registered',1,'http://nycattechnologies.test/users/1/edit','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/users/1','GET','2019-01-12 18:28:53','2019-01-12 18:28:53',NULL),(6,'Viewed users','Registered',1,'http://nycattechnologies.test/users','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/users/1/edit','GET','2019-01-12 18:29:13','2019-01-12 18:29:13',NULL),(7,'Viewed users/deleted','Registered',1,'http://nycattechnologies.test/users/deleted','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/users','GET','2019-01-12 18:29:22','2019-01-12 18:29:22',NULL),(8,'Viewed users','Registered',1,'http://nycattechnologies.test/users','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/users/deleted','GET','2019-01-12 18:29:25','2019-01-12 18:29:25',NULL),(9,'Viewed users/create','Registered',1,'http://nycattechnologies.test/users/create','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/users','GET','2019-01-12 18:29:33','2019-01-12 18:29:33',NULL),(10,'Viewed profile/roob.julian','Registered',1,'http://nycattechnologies.test/profile/roob.julian','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/users/create','GET','2019-01-12 18:29:57','2019-01-12 18:29:57',NULL),(11,'Viewed profile/roob.julian/edit','Registered',1,'http://nycattechnologies.test/profile/roob.julian/edit','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/profile/roob.julian','GET','2019-01-12 18:30:00','2019-01-12 18:30:00',NULL),(12,'Logged Out','Registered',1,'http://nycattechnologies.test/logout','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/profile/roob.julian/edit','POST','2019-01-12 18:30:51','2019-01-12 18:30:51',NULL),(13,'Logged In','Registered',1,'http://nycattechnologies.test/login','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/login','POST','2019-01-12 20:41:07','2019-01-12 20:41:07',NULL),(14,'Viewed home','Registered',1,'http://nycattechnologies.test/home','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/login','GET','2019-01-12 20:41:07','2019-01-12 20:41:07',NULL),(15,'Viewed themes','Registered',1,'http://nycattechnologies.test/themes','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/home','GET','2019-01-12 20:41:12','2019-01-12 20:41:12',NULL),(16,'Viewed themes/21/edit','Registered',1,'http://nycattechnologies.test/themes/21/edit','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/themes','GET','2019-01-12 20:41:38','2019-01-12 20:41:38',NULL),(17,'Edited themes/21','Registered',1,'http://nycattechnologies.test/themes/21','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/themes/21/edit','PUT','2019-01-12 20:41:52','2019-01-12 20:41:52',NULL),(18,'Viewed themes/21','Registered',1,'http://nycattechnologies.test/themes/21','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/themes/21/edit','GET','2019-01-12 20:41:53','2019-01-12 20:41:53',NULL),(19,'Viewed phpinfo','Registered',1,'http://nycattechnologies.test/phpinfo','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/themes/21','GET','2019-01-12 20:42:04','2019-01-12 20:42:04',NULL),(20,'Viewed routes','Registered',1,'http://nycattechnologies.test/routes','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/phpinfo','GET','2019-01-12 20:42:11','2019-01-12 20:42:11',NULL),(21,'Viewed active-users','Registered',1,'http://nycattechnologies.test/active-users','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/routes','GET','2019-01-12 20:42:27','2019-01-12 20:42:27',NULL),(22,'Viewed logs','Registered',1,'http://nycattechnologies.test/logs','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/active-users','GET','2019-01-12 20:42:33','2019-01-12 20:42:33',NULL),(23,'Viewed logs','Registered',1,'http://nycattechnologies.test/logs?l=bGFyYXZlbC0yMDE5LTAxLTEyLmxvZw%3D%3D','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/logs','GET','2019-01-12 20:42:47','2019-01-12 20:42:47',NULL),(24,'Viewed logs','Registered',1,'http://nycattechnologies.test/logs?del=bGFyYXZlbC5sb2c%3D','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/logs','GET','2019-01-12 20:43:23','2019-01-12 20:43:23',NULL),(25,'Viewed logs','Registered',1,'http://nycattechnologies.test/logs?delall=true','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/logs','GET','2019-01-12 20:43:48','2019-01-12 20:43:48',NULL),(26,'Viewed logs','Registered',1,'http://nycattechnologies.test/logs','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/logs','GET','2019-01-12 20:43:48','2019-01-12 20:43:48',NULL),(27,'Viewed home','Registered',1,'http://nycattechnologies.test/home','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/','GET','2019-01-12 20:44:05','2019-01-12 20:44:05',NULL),(28,'Logged Out','Registered',1,'http://nycattechnologies.test/logout','192.168.10.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9,es;q=0.8','http://nycattechnologies.test/home','POST','2019-01-12 20:44:11','2019-01-12 20:44:11',NULL);
/*!40000 ALTER TABLE `laravel_logger_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_15_105324_create_roles_table',1),(4,'2016_01_15_114412_create_role_user_table',1),(5,'2016_01_26_115212_create_permissions_table',1),(6,'2016_01_26_115523_create_permission_role_table',1),(7,'2016_02_09_132439_create_permission_user_table',1),(8,'2017_03_09_082449_create_social_logins_table',1),(9,'2017_03_09_082526_create_activations_table',1),(10,'2017_03_20_213554_create_themes_table',1),(11,'2017_03_21_042918_create_profiles_table',1),(12,'2017_11_04_103444_create_laravel_logger_activity_table',1),(13,'2017_12_09_070937_create_two_step_auth_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1,1,'2019-01-12 17:32:47','2019-01-12 17:32:47'),(2,2,1,'2019-01-12 17:32:47','2019-01-12 17:32:47'),(3,3,1,'2019-01-12 17:32:47','2019-01-12 17:32:47'),(4,4,1,'2019-01-12 17:32:47','2019-01-12 17:32:47');
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_user`
--

DROP TABLE IF EXISTS `permission_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_user_permission_id_index` (`permission_id`),
  KEY `permission_user_user_id_index` (`user_id`),
  CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_user`
--

LOCK TABLES `permission_user` WRITE;
/*!40000 ALTER TABLE `permission_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'Can View Users','view.users','Can view users','Permission','2019-01-12 17:32:46','2019-01-12 17:32:46'),(2,'Can Create Users','create.users','Can create new users','Permission','2019-01-12 17:32:46','2019-01-12 17:32:46'),(3,'Can Edit Users','edit.users','Can edit users','Permission','2019-01-12 17:32:47','2019-01-12 17:32:47'),(4,'Can Delete Users','delete.users','Can delete users','Permission','2019-01-12 17:32:47','2019-01-12 17:32:47');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `theme_id` int(10) unsigned NOT NULL DEFAULT '1',
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `twitter_username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `github_username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profiles_theme_id_foreign` (`theme_id`),
  KEY `profiles_user_id_index` (`user_id`),
  CONSTRAINT `profiles_theme_id_foreign` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`id`),
  CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` VALUES (1,1,1,NULL,NULL,NULL,NULL,NULL,0,'2019-01-12 17:32:48','2019-01-12 17:32:48'),(2,2,1,NULL,NULL,NULL,NULL,NULL,0,'2019-01-12 17:32:48','2019-01-12 17:32:48');
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_role_id_index` (`role_id`),
  KEY `role_user_user_id_index` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1,1,'2019-01-12 17:32:48','2019-01-12 17:32:48'),(2,2,2,'2019-01-12 17:32:48','2019-01-12 17:32:48');
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin','admin','Admin Role',5,'2019-01-12 17:32:47','2019-01-12 17:32:47'),(2,'User','user','User Role',1,'2019-01-12 17:32:47','2019-01-12 17:32:47'),(3,'Unverified','unverified','Unverified Role',0,'2019-01-12 17:32:47','2019-01-12 17:32:47');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_logins`
--

DROP TABLE IF EXISTS `social_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_logins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `provider` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `social_logins_user_id_index` (`user_id`),
  CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_logins`
--

LOCK TABLES `social_logins` WRITE;
/*!40000 ALTER TABLE `social_logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `themes`
--

DROP TABLE IF EXISTS `themes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `taggable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `taggable_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `themes_name_unique` (`name`),
  UNIQUE KEY `themes_link_unique` (`link`),
  KEY `themes_taggable_type_taggable_id_index` (`taggable_type`,`taggable_id`),
  KEY `themes_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `themes`
--

LOCK TABLES `themes` WRITE;
/*!40000 ALTER TABLE `themes` DISABLE KEYS */;
INSERT INTO `themes` VALUES (1,'Default','null',NULL,1,'theme',1,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(2,'Darkly','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/darkly/bootstrap.min.css',NULL,1,'theme',2,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(3,'Cyborg','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/cyborg/bootstrap.min.css',NULL,1,'theme',3,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(4,'Cosmo','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/cosmo/bootstrap.min.css',NULL,1,'theme',4,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(5,'Cerulean','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/cerulean/bootstrap.min.css',NULL,1,'theme',5,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(6,'Flatly','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/flatly/bootstrap.min.css',NULL,1,'theme',6,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(7,'Journal','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/journal/bootstrap.min.css',NULL,1,'theme',7,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(8,'Lumen','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/lumen/bootstrap.min.css',NULL,1,'theme',8,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(9,'Paper','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/paper/bootstrap.min.css',NULL,1,'theme',9,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(10,'Readable','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/readable/bootstrap.min.css',NULL,1,'theme',10,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(11,'Sandstone','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/sandstone/bootstrap.min.css',NULL,1,'theme',11,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(12,'Simplex','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/simplex/bootstrap.min.css',NULL,1,'theme',12,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(13,'Slate','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/slate/bootstrap.min.css',NULL,1,'theme',13,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(14,'Spacelab','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/spacelab/bootstrap.min.css',NULL,1,'theme',14,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(15,'Superhero','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/superhero/bootstrap.min.css',NULL,1,'theme',15,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(16,'United','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/united/bootstrap.min.css',NULL,1,'theme',16,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(17,'Yeti','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/yeti/bootstrap.min.css',NULL,1,'theme',17,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(18,'Bootstrap 4.0.0 Alpha','https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css',NULL,1,'theme',18,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(19,'Materialize','https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css',NULL,1,'theme',19,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(20,'Bootstrap Material Design 4.0.0','https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/4.0.0/bootstrap-material-design.min.css',NULL,1,'theme',20,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(21,'Bootstrap Material Design 4.0.2','https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/4.0.2/bootstrap-material-design.min.css',NULL,1,'theme',21,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(22,'mdbootstrap','https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.1/css/mdb.min.css',NULL,1,'theme',22,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(23,'Litera','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/litera/bootstrap.min.css',NULL,1,'theme',23,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(24,'Lux','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/lux/bootstrap.min.css',NULL,1,'theme',24,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(25,'Materia','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/materia/bootstrap.min.css',NULL,1,'theme',25,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(26,'Minty','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/minty/bootstrap.min.css',NULL,1,'theme',26,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(27,'Pulse','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/pulse/bootstrap.min.css',NULL,1,'theme',27,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(28,'Sketchy','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/sketchy/bootstrap.min.css',NULL,1,'theme',28,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(29,'Solar','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/solar/bootstrap.min.css',NULL,1,'theme',29,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL);
/*!40000 ALTER TABLE `themes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `signup_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signup_confirmation_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signup_sm_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_name_unique` (`name`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'roob.julian','Marcella','Wolf','admin@admin.com','$2y$10$JXP3I2SxpW6Y9lWG6jUPbOvIi.KTjFl7RGmyQoNre9N1f0PjzDzEC','wOjvAindQUkM9CMiZREmNOjJSJ6hLNchgoXsS2BVtw43c7a4E9oOUGpo3LAn',1,'z5IF0LwSShAI4AY80RK9xDUBVUlDl07LOmrVbqLKdNMUeF7g6k3ufmRrmHiRcPfW',NULL,'5.233.82.131',NULL,'255.17.54.82',NULL,NULL,'2019-01-12 17:32:47','2019-01-12 17:32:47',NULL),(2,'grover09','Arjun','Stracke','user@user.com','$2y$10$4Z7wvEtLSnFSX1DuK2P78ukEw0h9XxxsGBiKGy.t.srTFT9QYQyfG',NULL,1,'DWJ6JlqVg4f1GZmx9EQSjnZsI5e7PAKmJEZ4tvINHo612bWRePcjYGUf6akm9VHW','130.61.90.182','187.196.35.114',NULL,NULL,NULL,NULL,'2019-01-12 17:32:48','2019-01-12 17:32:48',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-28  0:02:38
