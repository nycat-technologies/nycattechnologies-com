# nycattechnologies.com
first install vagrant installation preffered:

'https://laravel.com/docs/5.7/homestead'

1. Run git clone 'https://gitlab.com/nycat-technologies/nycattechnologies-com.git'
2. Create a MySQL database for the project
* ```mysql -u root -p```, if using Vagrant: ```mysql -u homestead -psecret```
    * ```create database nycattechnolgies;```
    * ```\q```

3. From the projects root run cp .env.example .env
4. Configure your .env file
5. Run composer update from the projects root folder
6. From the projects root folder run `sudo chmod -R 755 ../nycattechnologies.com`
7. From the projects root folder run `php artisan key:generate`
8. From the projects root folder run `php artisan migrate`
9. From the projects root folder run `composer dump-autoload`
10. From the projects root folder run `php artisan db:seed`



#### Build the Front End Assets with Mix
##### Using NPM:
1. From the projects root folder run `npm install`
2. From the projects root folder run `npm run dev` or `npm run production`
  * You can watch assets with `npm run watch`

##### Using Yarn:
1. From the projects root folder run `yarn install`
2. From the projects root folder run `yarn run dev` or `yarn run production`
  * You can watch assets with `yarn run watch`

### Seeds
##### Seeded Roles
  * Unverified - Level 0
  * User  - Level 1
  * Administrator - Level 5

##### Seeded Permissions
  * view.users
  * create.users
  * edit.users
  * delete.users

##### Seeded Users

|Email|Password|Access|
|:------------|:------------|:------------|
|user@user.com|password|User Access|
|admin@admin.com|password|Admin Access|

### Routes

```
+--------+----------------------------------------+---------------------------------------+----------------------------------+---------------------------------------------------------------------------------------------------+-------------------------------------------------+
| Domain | Method                                 | URI                                   | Name                             | Action                                                                                            | Middleware                                      |
+--------+----------------------------------------+---------------------------------------+----------------------------------+---------------------------------------------------------------------------------------------------+-------------------------------------------------+
|        | GET|HEAD                               | /                                     | welcome                          | App\Http\Controllers\WelcomeController@welcome                                                    | web                                             |
|        | GET|HEAD                               | _debugbar/assets/javascript           | debugbar.assets.js               | Barryvdh\Debugbar\Controllers\AssetController@js                                                  | Barryvdh\Debugbar\Middleware\DebugbarEnabled    |
|        | GET|HEAD                               | _debugbar/assets/stylesheets          | debugbar.assets.css              | Barryvdh\Debugbar\Controllers\AssetController@css                                                 | Barryvdh\Debugbar\Middleware\DebugbarEnabled    |
|        | DELETE                                 | _debugbar/cache/{key}/{tags?}         | debugbar.cache.delete            | Barryvdh\Debugbar\Controllers\CacheController@delete                                              | Barryvdh\Debugbar\Middleware\DebugbarEnabled    |
|        | GET|HEAD                               | _debugbar/clockwork/{id}              | debugbar.clockwork               | Barryvdh\Debugbar\Controllers\OpenHandlerController@clockwork                                     | Barryvdh\Debugbar\Middleware\DebugbarEnabled    |
|        | GET|HEAD                               | _debugbar/open                        | debugbar.openhandler             | Barryvdh\Debugbar\Controllers\OpenHandlerController@handle                                        | Barryvdh\Debugbar\Middleware\DebugbarEnabled    |
|        | GET|HEAD                               | activate                              | activate                         | App\Http\Controllers\Auth\ActivateController@initial                                              | web,activity,auth                               |
|        | GET|HEAD                               | activate/{token}                      | authenticated.activate           | App\Http\Controllers\Auth\ActivateController@activate                                             | web,activity,auth                               |
|        | GET|HEAD                               | activation                            | authenticated.activation-resend  | App\Http\Controllers\Auth\ActivateController@resend                                               | web,activity,auth                               |
|        | GET|HEAD                               | activation-required                   | activation-required              | App\Http\Controllers\Auth\ActivateController@activationRequired                                   | web,auth,activated,activity                     |
|        | GET|HEAD                               | active-users                          |                                  | App\Http\Controllers\AdminDetailsController@activeUsers                                           | web,auth,activated,role:admin,activity,twostep  |
|        | GET|HEAD                               | activity                              | activity                         | jeremykenedy\LaravelLogger\App\Http\Controllers\LaravelLoggerController@showAccessLog             | web,auth,activity,role:admin                    |
|        | DELETE                                 | activity/clear-activity               | clear-activity                   | jeremykenedy\LaravelLogger\App\Http\Controllers\LaravelLoggerController@clearActivityLog          | web,auth,activity,role:admin                    |
|        | GET|HEAD                               | activity/cleared                      | cleared                          | jeremykenedy\LaravelLogger\App\Http\Controllers\LaravelLoggerController@showClearedActivityLog    | web,auth,activity,role:admin                    |
|        | GET|HEAD                               | activity/cleared/log/{id}             |                                  | jeremykenedy\LaravelLogger\App\Http\Controllers\LaravelLoggerController@showClearedAccessLogEntry | web,auth,activity,role:admin                    |
|        | DELETE                                 | activity/destroy-activity             | destroy-activity                 | jeremykenedy\LaravelLogger\App\Http\Controllers\LaravelLoggerController@destroyActivityLog        | web,auth,activity,role:admin                    |
|        | GET|HEAD                               | activity/log/{id}                     |                                  | jeremykenedy\LaravelLogger\App\Http\Controllers\LaravelLoggerController@showAccessLogEntry        | web,auth,activity,role:admin                    |
|        | POST                                   | activity/restore-log                  | restore-activity                 | jeremykenedy\LaravelLogger\App\Http\Controllers\LaravelLoggerController@restoreClearedActivityLog | web,auth,activity,role:admin                    |
|        | POST                                   | avatar/upload                         | avatar.upload                    | App\Http\Controllers\ProfilesController@upload                                                    | web,auth,activated,currentUser,activity,twostep |
|        | POST                                   | broadcasting/auth                     |                                  | Illuminate\Broadcasting\BroadcastController@authenticate                                          | web                                             |
|        | GET|HEAD                               | exceeded                              | exceeded                         | App\Http\Controllers\Auth\ActivateController@exceeded                                             | web,activity,auth                               |
|        | GET|HEAD                               | home                                  | public.home                      | App\Http\Controllers\UserController@index                                                         | web,auth,activated,activity,twostep             |
|        | GET|HEAD                               | images/profile/{id}/avatar/{image}    |                                  | App\Http\Controllers\ProfilesController@userProfileAvatar                                         | web,auth,activated,currentUser,activity,twostep |
|        | POST                                   | login                                 |                                  | App\Http\Controllers\Auth\LoginController@login                                                   | web,guest                                       |
|        | GET|HEAD                               | login                                 | login                            | App\Http\Controllers\Auth\LoginController@showLoginForm                                           | web,guest                                       |
|        | GET|HEAD                               | logout                                | logout                           | App\Http\Controllers\Auth\LoginController@logout                                                  | web,auth,activated,activity                     |
|        | POST                                   | logout                                | logout                           | App\Http\Controllers\Auth\LoginController@logout                                                  | web                                             |
|        | GET|HEAD                               | logs                                  |                                  | Rap2hpoutre\LaravelLogViewer\LogViewerController@index                                            | web,auth,activated,role:admin,activity,twostep  |
|        | POST                                   | password/email                        | password.email                   | App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail                             | web,guest                                       |
|        | POST                                   | password/reset                        |                                  | App\Http\Controllers\Auth\ResetPasswordController@reset                                           | web,guest                                       |
|        | GET|HEAD                               | password/reset                        | password.request                 | App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm                            | web,guest                                       |
|        | GET|HEAD                               | password/reset/{token}                | password.reset                   | App\Http\Controllers\Auth\ResetPasswordController@showResetForm                                   | web,guest                                       |
|        | GET|HEAD|POST|PUT|PATCH|DELETE|OPTIONS | php                                   |                                  | Illuminate\Routing\RedirectController                                                             | web                                             |
|        | GET|HEAD                               | phpinfo                               | laravelPhpInfo::phpinfo          | jeremykenedy\LaravelPhpInfo\App\Http\Controllers\LaravelPhpInfoController@phpinfo                 | web,auth,activated,role:admin,activity,twostep  |
|        | GET|HEAD                               | profile/create                        | profile.create                   | App\Http\Controllers\ProfilesController@create                                                    | web,auth,activated,currentUser,activity,twostep |
|        | GET|HEAD                               | profile/{profile}                     | profile.show                     | App\Http\Controllers\ProfilesController@show                                                      | web,auth,activated,currentUser,activity,twostep |
|        | PUT|PATCH                              | profile/{profile}                     | profile.update                   | App\Http\Controllers\ProfilesController@update                                                    | web,auth,activated,currentUser,activity,twostep |
|        | GET|HEAD                               | profile/{profile}/edit                | profile.edit                     | App\Http\Controllers\ProfilesController@edit                                                      | web,auth,activated,currentUser,activity,twostep |
|        | GET|HEAD                               | profile/{username}                    | {username}                       | App\Http\Controllers\ProfilesController@show                                                      | web,auth,activated,activity,twostep             |
|        | DELETE                                 | profile/{username}/deleteUserAccount  | {username}                       | App\Http\Controllers\ProfilesController@deleteUserAccount                                         | web,auth,activated,currentUser,activity,twostep |
|        | PUT                                    | profile/{username}/updateUserAccount  | {username}                       | App\Http\Controllers\ProfilesController@updateUserAccount                                         | web,auth,activated,currentUser,activity,twostep |
|        | PUT                                    | profile/{username}/updateUserPassword | {username}                       | App\Http\Controllers\ProfilesController@updateUserPassword                                        | web,auth,activated,currentUser,activity,twostep |
|        | GET|HEAD                               | re-activate/{token}                   | user.reactivate                  | App\Http\Controllers\RestoreUserController@userReActivate                                         | web,activity                                    |
|        | GET|HEAD                               | register                              | register                         | App\Http\Controllers\Auth\RegisterController@showRegistrationForm                                 | web,guest                                       |
|        | POST                                   | register                              |                                  | App\Http\Controllers\Auth\RegisterController@register                                             | web,guest                                       |
|        | GET|HEAD                               | routes                                |                                  | App\Http\Controllers\AdminDetailsController@listRoutes                                            | web,auth,activated,role:admin,activity,twostep  |
|        | POST                                   | search-users                          | search-users                     | App\Http\Controllers\UsersManagementController@search                                             | web,auth,activated,role:admin,activity,twostep  |
|        | GET|HEAD                               | social/handle/{provider}              | social.handle                    | App\Http\Controllers\Auth\SocialController@getSocialHandle                                        | web,activity                                    |
|        | GET|HEAD                               | social/redirect/{provider}            | social.redirect                  | App\Http\Controllers\Auth\SocialController@getSocialRedirect                                      | web,activity                                    |
|        | POST                                   | themes                                | themes.store                     | App\Http\Controllers\ThemesManagementController@store                                             | web,auth,activated,role:admin,activity,twostep  |
|        | GET|HEAD                               | themes                                | themes                           | App\Http\Controllers\ThemesManagementController@index                                             | web,auth,activated,role:admin,activity,twostep  |
|        | GET|HEAD                               | themes/create                         | themes.create                    | App\Http\Controllers\ThemesManagementController@create                                            | web,auth,activated,role:admin,activity,twostep  |
|        | DELETE                                 | themes/{theme}                        | themes.destroy                   | App\Http\Controllers\ThemesManagementController@destroy                                           | web,auth,activated,role:admin,activity,twostep  |
|        | PUT|PATCH                              | themes/{theme}                        | themes.update                    | App\Http\Controllers\ThemesManagementController@update                                            | web,auth,activated,role:admin,activity,twostep  |
|        | GET|HEAD                               | themes/{theme}                        | themes.show                      | App\Http\Controllers\ThemesManagementController@show                                              | web,auth,activated,role:admin,activity,twostep  |
|        | GET|HEAD                               | themes/{theme}/edit                   | themes.edit                      | App\Http\Controllers\ThemesManagementController@edit                                              | web,auth,activated,role:admin,activity,twostep  |
|        | GET|HEAD                               | users                                 | users                            | App\Http\Controllers\UsersManagementController@index                                              | web,auth,activated,role:admin,activity,twostep  |
|        | POST                                   | users                                 | users.store                      | App\Http\Controllers\UsersManagementController@store                                              | web,auth,activated,role:admin,activity,twostep  |
|        | GET|HEAD                               | users/create                          | users.create                     | App\Http\Controllers\UsersManagementController@create                                             | web,auth,activated,role:admin,activity,twostep  |
|        | GET|HEAD                               | users/deleted                         | deleted.index                    | App\Http\Controllers\SoftDeletesController@index                                                  | web,auth,activated,role:admin,activity,twostep  |
|        | DELETE                                 | users/deleted/{deleted}               | deleted.destroy                  | App\Http\Controllers\SoftDeletesController@destroy                                                | web,auth,activated,role:admin,activity,twostep  |
|        | PUT|PATCH                              | users/deleted/{deleted}               | deleted.update                   | App\Http\Controllers\SoftDeletesController@update                                                 | web,auth,activated,role:admin,activity,twostep  |
|        | GET|HEAD                               | users/deleted/{deleted}               | deleted.show                     | App\Http\Controllers\SoftDeletesController@show                                                   | web,auth,activated,role:admin,activity,twostep  |
|        | DELETE                                 | users/{user}                          | user.destroy                     | App\Http\Controllers\UsersManagementController@destroy                                            | web,auth,activated,role:admin,activity,twostep  |
|        | PUT|PATCH                              | users/{user}                          | users.update                     | App\Http\Controllers\UsersManagementController@update                                             | web,auth,activated,role:admin,activity,twostep  |
|        | GET|HEAD                               | users/{user}                          | users.show                       | App\Http\Controllers\UsersManagementController@show                                               | web,auth,activated,role:admin,activity,twostep  |
|        | GET|HEAD                               | users/{user}/edit                     | users.edit                       | App\Http\Controllers\UsersManagementController@edit                                               | web,auth,activated,role:admin,activity,twostep  |
|        | GET|HEAD                               | verification/needed                   | laravel2step::verificationNeeded | jeremykenedy\laravel2step\App\Http\Controllers\TwoStepController@showVerification                 | web,auth,Closure                                |
|        | POST                                   | verification/resend                   | laravel2step::resend             | jeremykenedy\laravel2step\App\Http\Controllers\TwoStepController@resend                           | web,auth,Closure                                |
|        | POST                                   | verification/verify                   | laravel2step::verify             | jeremykenedy\laravel2step\App\Http\Controllers\TwoStepController@verify                           | web,auth,Closure                                |
+--------+----------------------------------------+---------------------------------------+----------------------------------+---------------------------------------------------------------------------------------------------+-------------------------------------------------+
```

### Socialite

#### Get Socialite Login API Keys:
* [Google Captcha API](https://www.google.com/recaptcha/admin#list)
* [Facebook API](https://developers.facebook.com/)
* [Twitter API](https://apps.twitter.com/)
* [Google &plus; API](https://console.developers.google.com/)
* [GitHub API](https://github.com/settings/applications/new)
* [YouTube API](https://developers.google.com/youtube/v3/getting-started)
* [Twitch TV API](https://www.twitch.tv/kraken/oauth2/clients/new)
* [Instagram API](https://instagram.com/developer/register/)
* [37 Signals API](https://github.com/basecamp/basecamp-classic-api)